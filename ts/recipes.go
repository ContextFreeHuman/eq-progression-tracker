/**
recipes.go

Last Modified: August 18, 2019
**/

package ts

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var (
	// RecipeDBs is the database of all known recipes (that count towards 350)
	RecipeDBs []map[int]Recipe

	// DBs is a slice containing the names of each of the tradeskills
	DBs = []string{"alchemy", "baking", "blacksmithing", "brewing", "fishing", "fletching", "jewelrymaking", "poisonmaking", "pottery", "research", "tailoring", "tinkering"}
)

// Recipe represents a tradeskill recipe and its details.
type Recipe struct { // TODO: A database of more than just recipe names and ids
	ID, Trivial int
	Name, skill string
	Ingredients []int
}

// Import collects the list of valid (counts towards 350) recipes for a given tradeskill and loads it into memory.
func Import(filename string, skill string) (validRecipes map[int]Recipe) {
	recipes, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Error: Failed to open file at %v", filename)
		return
	}
	defer recipes.Close()
	validRecipes = make(map[int]Recipe)
	stepper := bufio.NewScanner(recipes)
	for stepper.Scan() {
		line := strings.Fields(stepper.Text())
		var itemName string
		ingredients := make([]int, 0) // Slice of item IDs
		i := 2
		id, err := strconv.Atoi(line[0])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer", line[0])
			err = nil // Continue
		}
		trivial, err := strconv.Atoi(line[1])
		if err != nil {
			fmt.Printf("Error: Failed to convert %v to an integer", line[1])
			err = nil // Continue
		}
		for ; line[i] != "["; i++ {
			itemName += " " + line[i]
		}
		i++
		for ; line[i] != "]"; i++ {
			itemID, err := strconv.Atoi(line[i])
			if err != nil {
				fmt.Printf("Error: Failed to convert %v to an integer", line[i])
				err = nil // Continue
			} else {
				ingredients = append(ingredients, itemID)
			}
		}
		validRecipes[id] = Recipe{id, trivial, itemName, skill, ingredients}
	}
	return validRecipes
}

// ImportDefault collects the lists of all valid (counts towards 350) recipes for all tradeskills and loads them into memory.
func ImportDefault() {
	allRecipes := make([]map[int]Recipe, 12)
	for i, skill := range DBs {
		allRecipes[i] = Import("ts/"+skill+"DB", skill)
	}
	RecipeDBs = allRecipes
}

// Compare returns the difference between a character's recipe list and the recipe database for a given tradeskill.
// The first slice refers to recipes the character does not know, while the second map refers to recipes the database does not store.
func Compare(ch map[int]Recipe, db map[int]Recipe) (unknown []int, new map[int]Recipe) {
	unknown, new = make([]int, 0), make(map[int]Recipe)
	for id := range db {
		_, ok := ch[id]
		if !ok { // Character is missing recipe
			unknown = append(unknown, id)
		}
	}
	for id := range ch { // TODO: Definitely don't need to go over DB twice
		_, ok := db[id]
		if !ok { // Database is missing recipe
			new[id] = ch[id]
		}
	}
	return unknown, new
}
