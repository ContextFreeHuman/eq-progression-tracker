# EQ Progression Tracker

As an older game that is growing ever more complex, [Everquest](https://www.everquest.com/home) 
tends to have aspects shrouded in mystery, as relevant online information 
disappears or becomes inaccurate.

This project is designed to address this issue by providing a repository of
up-to-date information and useful tools for players, as well as maintaining a
loose archive of changes to the game for use by emulators (among others).

### Roadmap (for original release):

- Adornment Manager & Database
- <del>Alternate Advancement Tracker</del>
- Known Tradeskill Recipe Trackers (for 350)

This project will depend heavily on data collected from the game. I will be
including ways to submit information for verification, but in the worst case,
I will be slowly collecting information from inside the game myself.

### Installation:
If you would like to try out the program, you can download a build (July 26th, 2019) 
with the resource files [here](https://mega.nz/#!cPA3zaBK!tNeG3S0RRalelfSa4YK-WU-4t8IiCOXcsME_NtNc39U) 
(hosted via Mega.nz). Just unzip the folder, run the .exe, and create a character!

In this build, only the AA functionality is available and there isn't much data - the data that
has been collected is largely for rogues.

If you'd like to build the executable yourself, you'll need a [current version of Go](https://golang.org) 
as well as the [Go bindings for GTK+3](https://github.com/gotk3/gotk3).

Please leave an issue if you run into any problems!